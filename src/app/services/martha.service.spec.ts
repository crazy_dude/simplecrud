import { TestBed } from '@angular/core/testing';

import { MarthaService } from './martha.service';

describe('MarthaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MarthaService = TestBed.get(MarthaService);
    expect(service).toBeTruthy();
  });
});
