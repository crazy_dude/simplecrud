import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { MarthaService } from './martha.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _currentUser: User;

  get currentUser(): User {
    return this._currentUser;
  }

  constructor(private martha: MarthaService) { 
    this._currentUser = JSON.parse(localStorage.getItem("CU"));
  }

  login(username: string, password: string): Observable<User> {
    return this.martha.select('login', { username, password }).pipe(
      map(users => {
        if (users && users.length === 1) {
          const userData = users[0];

          this._currentUser = new User (userData.id, userData.username);
          localStorage.setItem("CU", JSON.stringify(this._currentUser));
          return this._currentUser;
        } else {
          return null;
        }
      })
    );
  }

  logout(){
    this._currentUser = null;
    localStorage.setItem ("CU", null);
  }
}
