import {Injectable} from '@angular/core';
import {Item} from '../models/item.model';
import { Observable } from 'rxjs';
import { MarthaService } from './martha.service';
import { AuthService } from './auth.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  private _items: Item[];
  private idIncrement: number;

  get items(): Observable<Item[]> {
    const currentId = this.authService.currentUser.id;
    return this.martha.select('list-items', {'user_id': currentId}).pipe(
      map(items => items || [])
    )
  }

  constructor(private martha: MarthaService, private authService: AuthService) {
    this._items = JSON.parse(localStorage.getItem('ITEMS')) || [];
    this.idIncrement = +localStorage.getItem('AI') || 0;
  }

  create(name: string, description: string): Observable<Item> {
    const userId = this.authService.currentUser.id;
    return this.martha.insert('create-item', {name, description, userId}).pipe(
      map(id => {
        if (id) {
          return new Item(id, name, description);
        } else {
          return null;
        }
      })
    );
  }

  get(id: number): Item {
    return this._items.find(i => i.id === id);
  }

  delete(item: Item): Observable<boolean> {
    return this.martha.delete('delete-item', {'id': item.id});
  }
}
