import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MarthaService {

  constructor(private httpClient: HttpClient) { }

  private getUrl(query: string) {
    return `http://martha.jh.shawinigan.info/queries/${query}/execute`;
  }

  select(query: string, body: any): Observable<any> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response.success && response.data) {
          return response.data;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  insert(query: string, body: any): Observable<number> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response.success && response.lastInsertId) {
          return response.lastInsertId;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  delete(query: string, body: any): Observable<boolean> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response.success) {
          return response.success;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }
}
