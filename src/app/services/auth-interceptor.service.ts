import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {
  //Change to your Martha login
  private readonly auth = btoa('alexandre:fgf7e;');

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('AuthInterceptorService');

    const request = req.clone({ headers: req.headers.set('auth', this.auth)});

    return next.handle(request).pipe(
      tap((response: HttpResponse<any>) => {
        console.log(response);
      })
    );
  }
}
