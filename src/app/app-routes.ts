import {Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ItemsComponent} from './components/items/items.component';
import {AuthGardService} from './services/auth-gard.service';
import {ItemDetailComponent} from './components/item-detail/item-detail.component';

export const routes: Routes = [

  {
    path: '',
    component: LoginComponent
  },
  {
    path: '',
    canActivate: [
      AuthGardService
    ],
    children: [
      {
        path: 'items',
        component: ItemsComponent
      },
      {
        path: 'items/:id',
        component: ItemDetailComponent
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];
