import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import { HttpClientModule, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';

import {routes} from './app-routes';
import {AppComponent} from './app.component';
import {LoginComponent} from './components/login/login.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {ItemsComponent} from './components/items/items.component';
import {AuthService} from './services/auth.service';
import {NavComponent} from './components/nav/nav.component';
import {ItemCreateComponent} from './components/item-create/item-create.component';
import {AuthGardService} from './services/auth-gard.service';
import {ItemsService} from './services/items.service';
import { ItemRowComponent } from './components/item-row/item-row.component';
import { ItemDetailComponent } from './components/item-detail/item-detail.component';
import { MarthaService } from './services/martha.service';
import { AuthInterceptorService } from './services/auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NotFoundComponent,
    ItemsComponent,
    NavComponent,
    ItemCreateComponent,
    ItemRowComponent,
    ItemDetailComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    AuthService,
    AuthGardService,
    ItemsService,
    MarthaService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}
