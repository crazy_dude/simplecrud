import {Component, OnInit} from '@angular/core';
import {ItemsService} from '../../services/items.service';
import {Item} from '../../models/item.model';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  private items: Item[] = [];

  constructor(private itemService: ItemsService) {
    itemService.items.subscribe(items => this.items = items);
  }

  ngOnInit() {
  }

  itemCreated(newItem: Item) {
    this.items.push(newItem);
  }

  itemDeleted() {
    this.itemService.items.subscribe(items => this.items = items);
  }
}
