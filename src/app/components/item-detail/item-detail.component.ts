import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Item} from '../../models/item.model';
import {ItemsService} from '../../services/items.service';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css']
})
export class ItemDetailComponent implements OnInit {

  item: Item;


  constructor(route: ActivatedRoute, private itemService: ItemsService) {
    route.paramMap.subscribe(params => this.item = itemService.get(Number(params.get('id'))));
  }

  ngOnInit() {
  }

}
