import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {Item} from '../../models/item.model';
import {ItemsService} from '../../services/items.service';
import {Router} from '@angular/router';

@Component({
  selector: '[app-item-row]',
  templateUrl: './item-row.component.html',
  styleUrls: ['./item-row.component.css']
})
export class ItemRowComponent implements OnInit {
  @Output() itemDelete: EventEmitter<any> = new EventEmitter();
  @Input() item: Item;

  constructor(private itemsService: ItemsService, private router: Router) {
  }

  ngOnInit() {
  }

  delete() {
    this.itemsService.delete(this.item).subscribe(success => this.itemDelete.emit());
  }

  view() {
    this.router.navigate(['/items/', this.item.id]);
  }
}
